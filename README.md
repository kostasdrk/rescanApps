**Research**
-
These application images were used to evaluate [ReScan](https://gitlab.com/kostasdrk/rescan/).

You can find more details on ReScan's methodology, architecture etc. in our research paper:

**_"ReScan: A Middleware Framework for Realistic and Robust Black-box Web Application Scanning"_** Kostas Drakonakis, Sotiris Ioannidis and Jason Polakis, NDSS 2023.

If you use ReScan or the applications in your research please [cite](https://dblp.org/rec/conf/ndss/DrakonakisIP23.html?view=bibtex) our paper.

**Setup**
-
* You will need `docker ` and `docker-compose`
* Download the corresponding app version from the original website and extract it under: **`./<app_dir>/web/<app_version>`**, e.g., get wordpress 5.1 and extract it in `./wordpress/web/wordpress_5.1`
    * You might have to setup the app first locally and then copy the installed application directory.
* To build & launch the image: `$ ./boot.sh <app_dir>`
* See `./<app_dir>/docker-compose.yml` for port mappings, e.g., wordpress will run on `http://127.0.0.1:8098`
* For some apps, you might want to change the `./web` owner to `www-data`: see `setup.sh`
