-- MySQL dump 10.13  Distrib 5.7.32, for Linux (x86_64)
--
-- Host: localhost    Database: scarf_db
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE USER 'root'@'%' IDENTIFIED BY 'root';
GRANT ALL PRIVILEGES ON scarf_db.* TO 'root'@'%';
FLUSH PRIVILEGES;

CREATE DATABASE IF NOT EXISTS `scarf_db` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `scarf_db`;

--
-- Table structure for table `authors`
--

DROP TABLE IF EXISTS `authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authors` (
  `paper_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`paper_id`,`order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authors`
--

LOCK TABLES `authors` WRITE;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` VALUES (1,1,0),(1,2,1);
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `paper_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ext` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` longblob NOT NULL,
  PRIMARY KEY (`paper_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `options` (
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('text','file','boolean') COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `options`
--

LOCK TABLES `options` WRITE;
/*!40000 ALTER TABLE `options` DISABLE KEYS */;
INSERT INTO `options` VALUES ('Background Image','file','images/container.jpg'),('Banner Image','file','images/logo.jpg'),('Conference Name','text','SuperSec'),('Is Forum Moderated (emails the admins on every post)','boolean','0'),('Style File (CSS)','file','style.css');
/*!40000 ALTER TABLE `options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `papers`
--

DROP TABLE IF EXISTS `papers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `papers` (
  `paper_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abstract` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pdf` longblob NOT NULL,
  `pdfname` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`paper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `papers`
--

LOCK TABLES `papers` WRITE;
/*!40000 ALTER TABLE `papers` DISABLE KEYS */;
INSERT INTO `papers` VALUES (1,'Sec Paper For the Sec Conf','This is our super dupe abstract.',_binary '%PDF-1.4\n%äüöß\n2 0 obj\n<</Length 3 0 R/Filter/FlateDecode>>\nstream\nx�=�\�\n1E����v��\�0\�~�\��\0���߷��d�{r\"X\�\r��²�G\n\�\�\r�^\�Y�\�AS!��0X�r�bVhl���8�\�(O�vN�3J\�$o\�z6\�\�Сw\���$ɝj�O�\�Q�\�N�f��˒(\n\nendstream\nendobj\n\n3 0 obj\n134\nendobj\n\n5 0 obj\n<</Length 6 0 R/Filter/FlateDecode/Length1 23164>>\nstream\nx�\�|y|[Օ���=-v�Ȼ�E\�Q�*^\�\�q\��BR7�;\���,+��m)�\�&a+`��ai)L1�\�m#\��\Ze�\�P\�a\�\�N�k����\�t(eZb���\�y#�ig~�>�_�\�w\�=�l��s\�}O�y(��BN��˿$13���������?ܞ��i\"Ӛ\��=�_x�\�\�#�\�#2�t\���\�\�J��.Q�{�W\�#�y4�(�m\�X\�ĥ\�W��r ����\�2��\�x\�U>�\�\�Z\�\�\\�qΠ\�\n�,/ށ�6\�\Z�lT{3\�G�*\���@�T%\�(\�k�a�?yK\�&�O$Y|�1�\�cfc���\�����|�S�lS�ZD�[���6퍾.\�D\�g\�ǚ��\�7觬�i4��LY�>\�f\�i#��\'d\�:KwR:u\�],��R&m��L��nf�D�Eߢ�\�\�\�\�S\�\�\�c��\"���MeTG�A��<���uG�Bf��\�Z\�\�2\�E?����\�v���\�>}Z\�\�jȫ�Fj�>��\�\�f��ᕸ\'\�6:��Qw\�K�i	�rG�\'\�ר��\�k�\r\�\�`S\�*�\�\�:�2\�V�Н�u�f	|�\�d8	M\�\Z�\Z�\�\�,��^1��l�M2R\Z��&/�\�j\�&���]�9]J\�\�{X�xO���.�n��}�9ʠ�X<{�=k�6\�z��\��\�o!#�h9<�zz\�\Zz��O�N\��iu@�,�i��	\�\��\�e��jw\�\�\�GaD\�8��g\���\��Kg�\���\�\���xI�G9���\�\�G\�o;\�GAz��\�~~�^bȯb\�\�3\�Ǿ\����\�a�6��jV�Q���5M���Kts�d��$]I\�ۯ\��ҏ\�����U��\�\�\�\�4{�\��%|������\�f\�6\�Y�V]�^����\��\�M&�i����o��\��OE�\�I��\"j�G�BV<H\'\�eH��J���k\��ih	�\�\�\��#�[��\�{	_˛�\�Ǉ᧫�\��h	\�S�\��U�;�GŠ,QV*{����2��R~�Z\�\"�B]�nQw�QD�\�p��\���\�q\�s�w��\�>�\��\�զk\��t�\�\�M\�t�txz�kF&]	O|�@\�E~\0����w�V��a�j\�\�\�\�&�)v�\��\�\�ؗ\�=\��-�\0k\�&\�\�\�������_˯\��x\�\�\�?\��3�<K�+e��Q١\\�a\rA\�r-<{������\����.VC\�\�\�\�\�\�Q�G�O\Z�~�p\�0e��\�\�Fn\�1\�+��1>b��\�hZij7\�h�\���,���rm~�\�\�؃��c<]=\�\�\0�\�TJ\�\��Cv\�P�2��$�yؖ��\�4�it�a�\�	�e/\�A#WP�\�\�a�\�\�\��E�c\�òՇ�!\�x=�jt�?\�O��t�\��K��\n�7\�#���\n��]\��8;\�ְϳ:v���g*\�Z��>�U\�6�w\�Uj}�\� [M������&��C}���\�o\�k\�Q�33D\�FuSP�\\�27#߯#Q�vb�\�~\�F0�DGŉb�3�S��w\�?\�-\�qd\�zT\�7��\�W\�_E\�\�\�a\�e��]?]���\��\�\�2\��xԒj\�\�v\�A}�yT�ۢ\�\�\�k���>�G���-cfc\��ਧ\�\��E��	��\��\�)0\�GS�[fe��\Z�\�a�\�\�1\�Q÷\r/\Z�\�\�\�\�=\�\�_\"�\�7��~Kbf\�&��Q\r\�]ۻh�w+\�P\�!?�l	\��z}%H�\Z޻��\�wP\'.�o\�+��,�\�\r�f\�i��w��!D�\Z6L�v�\�Nb�x���t�\�l��\ZގJ���.4�K \�O�)ꃆ�\�\�\��\'i5*k��O��Rf��l	�:�z�C�(�V~\�8-�\�]Ž\�38c���\��ʥ�\�^X��u����j���������Άuկ]�zU]m͊\�\�U�\�\�e�%\�E�K\�K\n4\�\���ܜlkVfFzZj�%9)1aQ|�\�d4�\ng��\�\�ڣ��z\�j�}Ær1���p\�C�5�Z҄�I�-�t�r��(�1J\�,%�h�T_�Lk�k\��\�\�$۱��-\��n-|F$|X�\n���X���0\�\�Z­��G[z�!n|Q|��\�_��\�\�\\(�e����uL<�e\�8\'s\"�\n\�؛[\�\��faAX)lq��۷v�4\�t�/�&��7L��\�d�$�&�&ll\n��\Z\�+VC7i\�˦Fo��Po�#�\�\�纬+������6���|\�:7��Ԧ�\�\�\�\�*�-V�&���\�kᩭ]�gĵ�2�\�[{F[��f8��C�6~]wW�]��X�XUl}{���|F\�\�\�\��G?Ӄ\�䌆i\���HN��X�4\�h��]��pC��\�՜7�N�\��Od;�\�3\�\�\�-)1ǎ\'%\�@B\�|�3;\'!I.��m��e\�\"�F$DXsk��ˎ5�\�*\Zu�^\�\\\�>D\��k\���x�6Z\�\�\�	`?��B�K\�-$�<�M5\�\��a�#\\V&R\�Ԅ�\�\�ur\\[�l\�$_i�[4tp�÷�\�5�pA��M�N\�\� |hkWl�Qon����\�0\�3S33\�\�̡��Y�;2��|\���f�%[2\�Z�ׄY\�\�L{b�m���;���\�ݷm�F��U�s:Nk\�Rr��\\E\�\")/�%����Z�F�\�}aI)Lk\r[z6Į\��\�3i2\�c���#�d7Ǧ[^\�X8^�`����Q��E��s\�\�h���V�\�\�V�\�:\�3Ꚍ\�k�\�1�0x\�\�\�3\�\�\��rí7wc�l\r��\��q;�a븓\�б�\�Oo7tvE8\�M=\�Ǘb�\�nE�\�g�b���1$z��\�T\�1\'\�!9�J��\'I�y\�\�=\�c8�\�\�U�\�|\�8�M��(g\�F\�$op��A�V(ޤN3\�6\r\�\\y�Qnv�duXޫ?[�\��n����\�\0\��.˫\nR\nR\nqa���@S�>p\Z\�/��SЅ\'2��e!t�(1�\�_�\�Q=	���\�Y2�3�3\r��V\�^�{�\�E[J!#��Z�l1g11\�dtj\"nQ\rL�ٙv��iU� %U#\�Re\�I~ӄ��\�\�i;�m�,;��\�ػ\�4�A\�H;���i\�Y���\�\�\�^�RVT׭\�5EE\�E�;x�S=WM��\�\�\�tM\�\�Ͼ\�J^�\\݆]��\�0\�+zn�\�>q͘��̦>�AmR\�%/<�ؓ�S�ĳ�-\�ߨ��ˤ{�Y�Jٓr�A�3f\�y}JoKy���\'yЙ�.ʤ�����8cZzQFM�RgR�S[Zs$�E3Yf��vf.YZs\�:f\�~\�;V�{+�\�/*�3��dЎ�\�;ff\�\�j��k\�;\��ߌ\�;\�C\�t�\�\"#Uo�\Z\�/���F�}IQQ-������z\�J*�\�<\���O�l۶�\r\�e,�����~\�.>6m=\�Y�%�:��\�\�\�:\�~�\�\�:*fk�Q)�ٙ\�`0\Z2��	5J��\�Zco\�-\�k�=AS*K;\�zJ�\�W�u\�æ��0>�.=Uz�4�J+K\�1q���Rc�3\'��\�Cr\�`*PM9��pY$\�T <�X5YRR�s��\��-E�)\��=)̗\�R&y�39\'�(?8_\�\�cy�-Dt|!*ɔ\� z\�J\�]\�bg#Z=\�\�\�b皋j*�_*~�XI.�*V�X+�*��\�\�%���Z�8�B����\�۹ׁD{o\�N\��\�\�\�\�\�p�\�KI]Mh˫ذc\�N���V��rEuf\�Jy\�\�@j��K�F	̀�r\�\�Z�,�@I����\�[\��WL���aec���j\�m�vn\�޹\�\�/�\�滾ZQ�ᦻ�9o�gǲ\�k\�>�b�4�U݆\�Lc5GSK,m2z\�iMH�1g&&ט\�\�(.�L\�p�-gM\r��&.J2Z8�\�4�*\n�\�\�z,\�2Ɏ8S%\'V&���Q�ѓ����2��Ոޙ���&#++G]�8�\�5�L�bg�#T?1J\�Ӊ3oe�FU�\�\�\��9ر\�l6��g=��\�\��k�c\��&˻�SÙ��\rҵ,uu��k\�j\0�$�\�\�&㲳-l\�h�Aŏ�:}�X��q\�\�xu\�D9F�\�o�I�)\ri��l\\R�\r�!�>�qLVwZAZZ3%)�%\�\�b\�\�%1\���\�>}cSaӧ�oݜ����\�\�j\�\�$������-I�Eb�[\��;�,�>j`2\�G��Fñ�cV\�b\�c�����&&%Q��.I&s\�9U/Ӗ_�ߓ\�\�?�oȷ$k,\�,Q���ٺ\'�^\�\�ʹڷ\"�@C�\�H7�\�g\�\�r\�׈�w\'�W��\��c�_\���\�?���}M�\�P;f8�Y�\�\�\'�)g�>7\�S\�\�\�ޟ�l�\�s-\�G\�\��A5\Z\nM��\�|\���Y�46s��U�\'�̦\�<�oSp��mQ\�\"�H�������@�z\0s\�ع�b�\�c5XHF�l_RΜ]\��\�\�k8���MT\'+�k\�˨\�\�*+S5��ҭ�\Z\�\�I+\�\�L\�驙)Ii\�dIJcd\�\�q\�\�Elע(e&\�\�R�c�XV�\�x\��������\�\�nV\�%�ʔ])<e��\�Ĥ�\"���\�2�2y&B�d\\BMfv\�Ǹ�\�\�{\�7�\���\�\�\�~��X�XZ.����By\�!mEm�(Y&Q22Vd\�Q\�\�{W\��\"PԴ\�\��\�\�7\�U�ڿpm\�\�\�XVom{�����\"\�n��\�@l\�\�>r�\�P\�q\�q�P\\8n*\�T\�\�\������qc@�	��L\�I��Ĩ\�N\�\"\rF�\Z\�MEL{9�`i��m֏\Z�~ �%\�\�\�\�`Î4�\�\�ng\�\�o�l�I�N�O�E8@\�<;\��r�\n�\�~\�6LN~o0\�=���1 07%\\)b4c	e�\�X�\�^\�k8�\�Vx�gr1Ϊt\�c_;F�\��\�\�E�\��J\�]�Gǟ�;�8�c6��\r�bck��ŏ$>i|2\��\�K�I�+	\��������\�\�ͯ\�p&�\�$g�\�x)C��.yq�쓲\��[�	\�I�\�I=I<ɚ\�Dd\�ְ�\�\�|�F�KJc��<\�[�d\�LNJ��\���+5�7�.J��\\�\�D�2�`KKʩ\\�k�o�}�\�\�\�f\'��9;\�\��Ʀ3r�l:�.��9g�\�Y�\�`u.N\�%ׂK^J�,j\rgeL��HƀH��}d�\�,���0�\�L\�g�.<�N\Z$\�_w�(\�ꓜ�R�P�$\�\'9\�,�BQ����C�v�#Ǯ\�ZhE5)��$M�&c�3��|\�\��\����ϰT\�Y�r�k��b\�K.��gl[\�W\�\�W�\���\�\�|��\rl\�ʃMM�1���\�\�{��q�\�]\��4g�\�C��~\�S�\�j�2�Cmc3��\�Mo���\�>�S�(\�w\��Wt�)��J\�q\�{	\�	ܗ�cd\��v\�0\����8���\�|Rє!\��\rj\��Y]z� \rċ#!*i;\�6[|\��K��\�ݻ4X^\�/G�\��+\�\�,:�R:\�\�aY\�\�t\��6\�w\�7t\�LEܯ\�q4\�\�\�\�x�9E\�\�E\�k��\'\�ns�\'\Z��\�\�$�,�ӳ~;�|\\�\�6\�j\�Z\�\�B\�,\�uX�xː(�r�_�\�&\�\�a3�Y��\�qԒb\�\�x\�J��/�\�i�\�~[�\"\�:��\�HO\�\�$�\��\��\n�\'e}I�U\�\�zP\�\�\�N\�J�Yߗ�xc\�k:�Rj\�6��d�I�����\��\'X\�tX%�\�&\�8_k�#�ٵ:9\�\r:��f_�Ð�}�#�\�:��f��#�ٿ\�a\�7\�aF|s^\�a\�7�S:��j\�:��j�\�a\�W�_:��\�)\�x\�\�\�a��8�\�E���d\�J�KNk)٨ð�d���D\�xtX�����-R\�m:,\�|M\�i\�\�%\�\�0|^�]	�{J~�ð�\�M	g\0�^\�tX%�4C���V�A_\�$\�lI�S��^	\�(�M����H8_\�S:�ð��)	\�$��uXп,\�\"J\�\�a\�@\�%\\&�S��\��OY\�\�rQ	\�JuX��\�\���0\�/��c�\�*۪\��K�	1��:,�\�KXƥ\�~z�N\�O~�\�nr��F��uR��7���Ђ:���\�a�\�\�\�+)4`�_�Y\�]�CI���iԁ�\n\�\��ۈ>�o9�ƻ�\�u�Zb�1�~x����\�\�y�aڇk��1\�\�z��\�;\�̣\�f�\�\�%RJ`\�\�\�Z���*�	X7f}����A*=����1G[>Ϯ\�y�oI\�\n��A\� �a�8�\��\�s\rX�\�MAi�𑆱�	\�R�#\Z�K~����M�n�\�\�\��.\�>�\no�HN!�\�<6\�\�\�^a���?�\�#�KЍH��\�\��\�\�[.�\�^\�\�\�r�_z\�k�\�\�>,g�2S;p\rI�c�e\�*\�R��$(�<\�aآ�ʥ\�b,��\��}2\�D\�\rI]�\�\�\�e��m�sPJv�C����&�vI}n=\Z�au@��K�1Ʒ6�^=\��z=\�7���\�\�Dȥ\��\�4�a�U3��\�)�^6Z��\�=��y;�{ĭgj\�� dz�W�\�c�\�:&$=-2j.�}r\�K�H~a��\�\�5��-��\�Z��Jc�QH��\�nP\ni1\�_��w}�J��>$GsQ\r\�,�֝?\'fjk`v-bnPʛ�!\�\�庵.\��nY�4}�\���O\�\�#�1~�üz�\��\�9\�\�U\�\�}��c檽K\�*����[_�WFm@\��\�ދe\�䌭d~v{g3K\��+�\�JkDn\�\��V�\�\�\�1(Gs\��Љ�\��ܺ�^)!$=ݷ 7=��ϊ\�vϮp�\�mM\��ҷ�w�\�z���=�߃zՈ����es\�36;(#\�+%\�j!\�-g\�2-��Oz\�/w\��\�U\�\��5S̻�\'�ubż��3\�H�\�\Z�usƶ\ny�1�gj%\�w���_a+du\ZE�\�K�\r\�y\�(@�d\�\"^1K�WÈ̘�g��ͨ��8�[њ�y\��8Zq��ķ\0Ӂ�\�͋q�\�Ib;)OS�u\�l\n�\'״Y|l�\�<\�\�}>��\�)6���<\�^9���Y�\�\�\�\�\��h~��U��:\Zۿ^�f�=�GJ�\�\�D�[�umbw\�\�ki\�\�i\��\�\�\�Α\�\�\�\�w�g6��e�\��y���\��\�\�.�̓2��\�\�ק��\"{ee�YݫGfH�|�\�U-�T�\"��\�j��m���佨Zto�\Z�Q����3Wg���~�1��+V�]\�\"���W�\��kb�\�84��\�\���Oz\�;\��w��l�zx^\�Ν\�\�)aݠ�?�W��Fd�/�ќ:S\�(}��ݡ��ǅ��\��\�욟݃zE��?���z~\�Uޅ9�q+�ˏ�r\�\�Fn\�\�K�9�-��\���[Fu\�C1���\�$\�\�jH\��\�Ρ}�\�h�\�\�]�\�џ�7�\��y�g��\�ŝǘ�\�\�X\�R\��x&b��z�\�d휗\�հ�_h�G��\r\�왑 �O\Z)�$P�{�\Z�\��\�r�\��|U#��ħ#۩M��\�\�r\�\�\�p�\�\�$\�J�ų�hB�\�v\���Oƙ�\�yo�<\�\�\��\�v�=ڣZg�G\�\���Қ|\�~߰+\��\ri�w�\�\�\n���J!L\��\r�&�m\��ի�\�q��\�\Z�m\�=����\��\���5{]\�}}32\�H�&Pk.��\�UUZ\�&�{\��\��Α̧�\�r)�Sh�î>Ϡk�rͷ�c-׆={���g\�ӧy�� H�wh���V�unҶ\�\�]����4\�@�3\���YIX�oϰ\�߿>ʣ5�F�C{�\�-׶�z!z�\�\�\�p�	\�\�^�ץu�BC}X\\���\�7�\nۆ�k�Gywk}��w\�\�2-�v7�\\^L��=Zh\�5�5w�k\�\�\�20�X�kH\�\�~�~/\�\�\�=nO \��:� \�\���W%\Z�h#\�`�tà\�\�\'���0\�\r�fp�\�P\�\��@hx�&=\�\�\�v!\��a�+8�)�\�!\��L\�\�3,M\�\Z\0(m��A�x��B��\\j ��3\�\"[B�gx\�;$)�}�C���CP`�׵\�\'\�G�\�s�\�3\��G|\�\�>�$�i\�\�\�m\�\�\ry\� w���q\�큒���\�Y�\�\n,f\�3�_\�\�ȝ!c\�; \�\�7R@\�\�G�G�Rқ��!al\�-��\��aɐ�E�\"O��a\�Dj L�L�\'���=�+�C\�	��Ŝ�>o�?\�\�/T\�!\�H�\��\�4���Ġ7 r��o\�\'�U���5��###�z\�V�}������\����ے\���.�Xx�@��#�`=�e�΍��\Z;7n٬mi\�>���esG�\�x񶖖M-�;\�\�;�\�\��	���P� (=z�-&#Y��w��\��n�m�\�G��Dr\�E|���@\�\�3\��L�к�\�\�B\Z�z\�6gp�1\";GD:y8����\�D�wÏsv���x$��,B�\�\�\r!\Zf���\�-�80cy\��\�\"۴}����\�\n C\�sWhۇd\�\�Y֤W.��K�=n/�ι+\�\�\�!�m�\�\�\�\�9���Uy�@K\�\�\��!���^� (�t#�\�\��$��(��\�P\�7\�/�@V\�݃HT؏P��k�\�\�=�P��\�\�\�s�\�ko\��jP�ܞ\�!}úݒ8\�\�\r�a\��zFb\�\�\�:D҃\n\�7W\�f\��dau\�b,\�ҭ\�}~�\�\�Y}\�날\�\\#�w4\�(YUSW�\�-_U^USU��\rȪ\�\�kjp�[Q�խ�]]�:1�#v\�\�nF1�\�͓����&I\�\�\���e\�L�B,�o-��\�sk:�U�\�c���SnP�Q���\�u|�����\r.|mp\�k�_\\�\�\�\�\��6�����\r.|mp\�k�_\\�\�\�\�\��6��k�\�\���Q�,\�f>�>��>�	-�=w�bY5�fp��Ɨ\�{����:,��ᙹ��W\�\�\�%�O\�l��}���g\��\�7$��|rWΧ>\��|O�>҇>զ�SתM\�Ju�\�T/R\�\�\��\�;\�y\�Ot氭\�\'�i#�4�\�\�m��\�\��x��\�/;�d\��,\�͛�\�7���\�+��\�S��~J\�y}�:��P2cd�N)_���W;\'��\'�Ӫ��\�NjG\�V6\�\Z\'�rD\� o��/�>&����j\�o\"\r\��Bc�29v�	��&\�2��k\"\�)�ﳑ��0a�V�7�+WS<\�\�ɦ@��}>�^��B\�\�H�T���7((C6�Qɤj�\�J\�J�P$)�\'))�n�W��$IVQ�l�Y1E�m\�	E�ٰS�a\"n��%��\�:\�D\�:�,[�3J<U���tN\�%VnLP:�\�N�Ŧ�����:��A_��G���\\ɧ��\�\�H�m\�r�$�;!�\�E\�+D7��T=\����+�\�\�Jm�\'�VUSc�RBU��	M9\��\�\�\�(�Q�i�EhFa\�(�1s#h*�+ɯ�\�a��\0����I`iI�1%[�\���6g\".IXf���I2\�DBRu\�3J���q�ȲV�N(er)\�&�����K�\�b�\0c���3J��Xz\"_z \�hØQ�b#\��O	\����E|\�O�\����E��a��N�S\�\�\��,�Ӎy�\r\�\�_��\0q~�?��\�\�\�\'��g�5�\�>�\�Я@<R�=\�$��@\�\�$f�\��\�#�J�\�@V��fV7�\����?E���|���?�ފ~�\�{\�൴�Q��Z\�4�?�3\�\�\'\"Ip\�$�#�\��ب�\��4��r@�\�HQ��L-�%��<\�\��H�-�1�\�Ϻػ \Z�WDO���H�r8�f;\��\�Nk��\�Y\�|H�*�*�zH\�\n�r�N{Hk��[\�\0\�a\��p\��̑=hN�\��ƈZn<�5�uq:�똄zp�K�p�\�ξ#�~mA\�q\0\� \�!��H\��J�Ϣ}\��D���|��\��~\�\��~p�%�_j�	�p�=��=\�\�G�\�����Gr���\�\�h�\�\�hG;8\�%G;8\��\�.9�\�p�\�	�\�p�\�	\'8��\�	\'8���\nU\�G�\�G8��Q%9��Q�*ɡ�C�Mrh\�\����C�\Z84ph�\�8,\�H8,ఀ\�\"9,2>!4�q\Z��q\Z�%\�ip�\�ip����q\Z��ȸr����\�)���,��r\n,��rJ��\�)��җ�\�\�H�h\��	\�)�N�w\n�S�wJ�WM���Ga\�Gap�%Gap�%\�8\��1�1\�1�1p��cLr�\�\�\r�	��=)�\�\��X��+?\�Je�ޖ�zE���q\����g\�j\�_Iu��\"\�C�\�d3���.�1%`\�.4\�}hG\�N��$�\�khQ^\�\\�&���\�31�4��N�x�q��>\�\�I�\��k��<Q\�Q����\���h8Dpm�P��\�\Z\�\�Z�kx�3\���2�R;YƎ��/��\�8~1Se�Ý>�\�˙P�\��\nZ]Q�:T�[�|;\�)Zi�dOǺR��\�h\�h�]�V�V�V�V�f��2\�w9�\�\"�F+F+@ӄ\n\�\�\�\�Mj�\�y�\'��&^H�8���|\'\"\�U\�&#\�[\�=)\�5Ʊ\'�X\��\'�\�\��\�^\��7c\�7\"�\��\�j\�\�W��4R���1�m\'�*X;��\����\��m�\�J\�9\"\�E���\n1[ʺ\�u�:\�Ҙ&{Ķݒ�m��6S�<3R�4π&ze��\�R�s�\�\�v\�\�`�����6��{�p�]⌷=]�U7\�\"��\�øއE��\��\�m�@+|\�v��\�vk��\�[`��RE\�v�6\�w�\�٪l��\�m\�\'l.\�6\�\�B\�#�\�lO3��u�ǟ��C\�F��0b��pR�\�j\�osڊm����iULn]�\�\�TӾ�-+�9��n��8\�L\��.5�7�5\�MKL�M��ts�\�bN2\'�\�\�f�Ѭ���\�\�\�\�\�\�+Ӎ\�\�ɨ��*aW�KS\�̜>A\�4���u�gm\�)7��j\��:\�,~뎰����Sۨ�s}x��m\�\��s��M\�v�3vk7�a~\�$�ήI�\�r\�Q�3�\�\�c\�X�u�tw�5s_��!u]\�\�\�\��\\z��c\�e�\�\�j\�\�\n?�\��@4��-|����c<�\'�4\�I�\�\�:��yr\�6�W�\�\� {]�!��@FŢ�y=i��d� C�btE`]�\�@�HE��(>QҩLЍ���4�k��)$zEҼRH�h�1\�m/*�Tv�u	*\�eפa�R�\��r�$���M\n�1�,\\9GR��\�Β\�J]\n���\�h\�Kfh\�K@\���<\�lby\���◿z\�-���M���\�C��6~ ��$XQO��_�.O8d�4�؛��\�ϟg�y1�\�\�<NϷtv�?\��4G�;��\�]\�\�\r�]�t\�8����<\�ꅰ.����<Ӎb�A\�j�\Z��g�\�\�\�y\�\�5n��\�wAd?�\�#�{r�\�gZ�\�DB[[`=�{\\%�-rt�\�\�Éhb����QLa���$��n����ڂ\�\�\�}\�t�}=͸�Q[�vk[��cG�H��\�u��\�KN[�\�یe\�{>%\��\n�\�\n�\�r\�)�-\\\�\�^���LP\�\�\�\r\\\�NQ$n<.�e2:�I�`A�N@&~#���.3���xTN\�\�W���	~\r\�q|$R)���\�ĒB�������x>}$��Z�AXE_\�)\�\0.?\\7V8V>Vg?�����\�Q\Z�|H��#0\���n��t�\�\�˗�\�\�pt;�7^\�îv\�#�\�:6�K\rH����\���\�&���\�\"\'C�E*�?X\�6\nendstream\nendobj\n\n6 0 obj\n10825\nendobj\n\n7 0 obj\n<</Type/FontDescriptor/FontName/BAAAAA+Arial-BoldMT\n/Flags 4\n/FontBBox[-627 -376 2000 1011]/ItalicAngle 0\n/Ascent 905\n/Descent 211\n/CapHeight 1010\n/StemV 80\n/FontFile2 5 0 R>>\nendobj\n\n8 0 obj\n<</Length 272/Filter/FlateDecode>>\nstream\nx�]�\�n� \�\�<\�\�aZu��1ٺ\�\�C����0Z���\�\�\�&=@~\�\�7�f`usm�r\�\�΢G���e^�\0\�è4IR*�p{���:C�׶\�\�`j�0�%ao>�8�\�\�E\�=\��b%X�Gz��[��1_0�v����\�\�3\�\�U\�F��r\�\�K�\n\�74\�8�V\�,a1�\0\�\�H\�yE\�ۭ\"�\�\\�K�A|v֗&���,�<�ȧ<�=rq\r�\��s\�9\�\��������\�u\�\�\�9�\�\��a�?k�b�֯\0�������\�1�	*<\�;��\�\nendstream\nendobj\n\n9 0 obj\n<</Type/Font/Subtype/TrueType/BaseFont/BAAAAA+Arial-BoldMT\n/FirstChar 0\n/LastChar 11\n/Widths[750 722 610 889 556 277 666 610 333 277 277 556 ]\n/FontDescriptor 7 0 R\n/ToUnicode 8 0 R\n>>\nendobj\n\n10 0 obj\n<<\n/F1 9 0 R\n>>\nendobj\n\n11 0 obj\n<</Font 10 0 R\n/ProcSet[/PDF/Text]>>\nendobj\n\n1 0 obj\n<</Type/Page/Parent 4 0 R/Resources 11 0 R/MediaBox[0 0 595 842]/Group<</S/Transparency/CS/DeviceRGB/I true>>/Contents 2 0 R>>\nendobj\n\n12 0 obj\n<</Count 1/First 13 0 R/Last 13 0 R\n>>\nendobj\n\n13 0 obj\n<</Title<FEFF00440075006D006D00790020005000440046002000660069006C0065>\n/Dest[1 0 R/XYZ 56.7 773.3 0]/Parent 12 0 R>>\nendobj\n\n4 0 obj\n<</Type/Pages\n/Resources 11 0 R\n/MediaBox[ 0 0 595 842 ]\n/Kids[ 1 0 R ]\n/Count 1>>\nendobj\n\n14 0 obj\n<</Type/Catalog/Pages 4 0 R\n/Outlines 12 0 R\n>>\nendobj\n\n15 0 obj\n<</Author<FEFF004500760061006E00670065006C006F007300200056006C006100630068006F006700690061006E006E00690073>\n/Creator<FEFF005700720069007400650072>\n/Producer<FEFF004F00700065006E004F00660066006900630065002E006F0072006700200032002E0031>\n/CreationDate(D:20070223175637+02\'00\')>>\nendobj\n\nxref\n0 16\n0000000000 65535 f \n0000011997 00000 n \n0000000019 00000 n \n0000000224 00000 n \n0000012330 00000 n \n0000000244 00000 n \n0000011154 00000 n \n0000011176 00000 n \n0000011368 00000 n \n0000011709 00000 n \n0000011910 00000 n \n0000011943 00000 n \n0000012140 00000 n \n0000012196 00000 n \n0000012429 00000 n \n0000012494 00000 n \ntrailer\n<</Size 16/Root 14 0 R\n/Info 15 0 R\n/ID [ <F7D77B3D22B9F92829D49FF5D78B8F28>\n<F7D77B3D22B9F92829D49FF5D78B8F28> ]\n>>\nstartxref\n12787\n%%EOF\n','dummy.pdf',1,1);
/*!40000 ALTER TABLE `papers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `starttime` datetime NOT NULL,
  `duration` int(11) NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (1,'Super Securiteh Session',2,'2022-01-04 01:25:00',540);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `affiliation` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `showemail` tinyint(1) NOT NULL DEFAULT '0',
  `privilege` enum('admin','user') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'abc@example.com','42870277bca861d8570ba90849ae82f9','Marianne','Dookienson','FoDiSCs',0,'user'),(2,'admin@example.com','5993fdd2eb713923083b498820f0084b','Jack','Russel','FORTH',0,'admin');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-10 18:43:33
