#!/bin/bash

apps=("wordpress" "wackopicko" "vanilla" "phpbb" "drupal" "hotcrp" "joomla" "oscommerce" "prestashop" "scarf")

if [ ! -z $1 ];
then
	for app in "$@";
	do
		cd "$app" && sudo docker-compose up --build &
	done
else
	echo "Need an app to start.."
	#for app in ${apps[@]};
	#do
	#	cd "$app" && sudo docker-compose up --build &
	#done
fi
