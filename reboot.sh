#!/bin/bash

if [ ! -z $1 ];
then
	for app in "$@";
        do
		if [ ! -d "$app" ];
		then
			echo "App does not exist: $app"
			exit 3
		fi
		container=$(echo "$app" | awk '{print tolower($0)}')
       	        sudo docker stop "lim_"$container"_db" && sudo docker rm "lim_"$container"_db"
                sudo docker stop "lim_"$container"_web" && sudo docker rm "lim_"$container"_web"
                sudo docker stop "lim_"$container"_admin" && sudo docker rm "lim_"$container"_admin"
                cd "$app" && sudo docker-compose up --build &
        done
else
	echo "Need to pass an app name..."
	exit 2
fi
